﻿using System.Linq;
using Castle.MicroKernel;
using Castle.Windsor;
using Demos.WebAPI.FrameworkExtensions;
using FluentAssertions;
using Xunit;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace Demos.WebAPI.Tests.FrameworkExtensions
{
    public class WindsorDependencyResolverSpecification : AutoMockSpecification<WindsorDependencyResolver, IDependencyResolver>
    {
        [Fact]
        public void When_dependency_resolver_gets_service()
        {
            var expected = Fake<object>();

            Chain<IWindsorContainer, IKernel>(x => x.Kernel);

            Dep<IKernel>(x => x.Setup(k => k.HasComponent(expected.GetType()))
                .Returns(true)
                .Verifiable());

            Dep<IWindsorContainer>(x => x.Setup(c => c.Resolve(expected.GetType()))
                                         .Returns(expected)
                                         .Verifiable());

            var result = Sut.GetService(expected.GetType());

            result.Should().NotBeNull();
            result.Should().Be(expected);

            Verify<IKernel>();
            Verify<IWindsorContainer>();
        }

        [Fact]
        public void When_dependency_resolver_gets_service_not_registered()
        {
            var serviceType = typeof (object);

            Chain<IWindsorContainer,IKernel>(x => x.Kernel);

            Dep<IKernel>(x => x.Setup(k => k.HasComponent(serviceType)).Returns(false).Verifiable());

            var result = Sut.GetService(serviceType);

            result.Should().BeNull();

            Verify<IKernel>();
        }

        [Fact]
        public void When_dependency_resolver_gets_all_services()
        {
            var services = FakeMany<object>();
            var serviceType = typeof (object);

            Dep<IWindsorContainer>(x => x.Setup(c => c.ResolveAll(serviceType)).Returns(services.ToArray()).Verifiable());

            var results = Sut.GetServices(serviceType);

            results.Should().BeEquivalentTo(services);

            Verify<IWindsorContainer>();
        }

        [Fact]
        public void When_dependency_resolver_begins_scope()
        {
            var result = Sut.BeginScope();

            result.Should().BeOfType<WindsorDependencyScope>();
        }
    }
}