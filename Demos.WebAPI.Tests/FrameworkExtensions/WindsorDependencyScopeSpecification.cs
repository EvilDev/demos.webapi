﻿using System.Linq;
using System.Web.Http.Dependencies;
using Castle.MicroKernel;
using Castle.Windsor;
using Demos.WebAPI.FrameworkExtensions;
using FluentAssertions;
using Xunit;

namespace Demos.WebAPI.Tests.FrameworkExtensions
{
    public class WindsorDependencyScopeSpecification : AutoMockSpecification<WindsorDependencyScope, IDependencyScope>
    {
        [Fact]
        public void When_scope_gets_service()
        {
            var expected = Fake<object>();

            Chain<IWindsorContainer, IKernel>(x => x.Kernel);

            Dep<IKernel>(x => x.Setup(k => k.HasComponent(expected.GetType())).Returns(true).Verifiable());

            Dep<IWindsorContainer>(x => x.Setup(c => c.Resolve(expected.GetType()))
                .Returns(expected)
                .Verifiable());

            var result = Sut.GetService(expected.GetType());

            result.Should().Be(expected);

            Verify<IWindsorContainer>();
        }

        [Fact]
        public void When_scope_gets_service_that_is_not_registered()
        {
            var expected = Fake<object>();

            Chain<IWindsorContainer, IKernel>(x => x.Kernel);

            Dep<IKernel>(x => x.Setup(k => k.HasComponent(expected.GetType())).Returns(false).Verifiable());

            var result = Sut.GetService(expected.GetType());

            result.Should().BeNull();

            Verify<IKernel>();
        }

        [Fact]
        public void When_scope_gets_services()
        {
            var expected = FakeMany<object>();

            var type = expected.First().GetType();

            Dep<IWindsorContainer>(x => x.Setup(c => c.ResolveAll(type)).Returns(expected.ToArray()));

            var result = Sut.GetServices(type);

            result.Should().BeEquivalentTo(expected);
        }
    }
}