﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Moq;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using Ploeh.AutoFixture.Dsl;
using Ploeh.AutoFixture.Kernel;

namespace Demos.WebAPI.Tests
{
    public abstract class AutoMockSpecification<TSut, TContract>
        where TSut : class, TContract
        where TContract : class
    {
        protected AutoMockSpecification(IFixture fixture)
        {
            Fixture = fixture.Customize(new AutoMoqCustomization());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            fixture.Customize<TSut>(x => x.FromFactory(new MethodInvoker(new GreedyConstructorQuery())));
        }

        protected AutoMockSpecification()
            : this(new Fixture())
        {
        }

        protected IFixture Fixture { get; private set; }

        protected T Dep<T>() where T : class
        {
            return Fixture.Freeze<Mock<T>>().Object;
        }

        protected T Dep<T>(Action<Mock<T>> setup) where T : class
        {
            var mock = Fixture.Freeze<Mock<T>>();
            setup(mock);
            return mock.Object;
        }

        protected T Register<T>(T instance) where T : class
        {
            Fixture.Register(() => instance);
            return instance;
        }

        protected void Chain<TParent, TChild>(Expression<Func<TParent, TChild>> expr)
            where TParent : class
            where TChild : class
        {
            Dep<TParent>(x => x.Setup(expr).Returns(Dep<TChild>()));
        }

        protected T Fake<T>()
        {
            return Fixture.Create<T>();
        }

        protected IEnumerable<T> FakeMany<T>()
        {
            return Fixture.CreateMany<T>();
        }

        protected T Fake<T>(Func<ICustomizationComposer<T>, IPostprocessComposer<T>> builder)
        {
            return builder(Fixture.Build<T>()).Create();
        }

        protected IEnumerable<T> FakeMany<T>(Func<ICustomizationComposer<T>, IPostprocessComposer<T>> builder)
        {
            return builder(Fixture.Build<T>()).CreateMany<T>();
        }

        protected void Verify<T>() where T : class
        {
            Dep<T>(x => x.VerifyAll());
        }

        protected TContract Sut
        {
            get { return Fixture.Freeze<TSut>(); }
        }
    }
}