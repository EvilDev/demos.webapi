﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.Filters;
using Castle.Core;
using FluentAssertions;
using Xunit;

namespace Demos.WebAPI.Tests
{
    public class ConfigureWebApiStartableSpecification : AutoMockSpecification<ConfigureWebApiStartable, IStartable>
    {
        [Fact]
        public void When_started()
        {
            var config = Register(Fake<HttpConfiguration>(x => x.Without(http => http.DependencyResolver)));
            var dependencyResolver = Dep<IDependencyResolver>();

            Sut.Start();

            config.DependencyResolver.Should().Be(dependencyResolver);
        }

        [Fact]
        public void Should_set_authorization_filter()
        {
            var config = Register(Fake<HttpConfiguration>());

            Sut.Start();

            config.Filters.Any(f => f.Instance is IAuthorizationFilter).Should().BeTrue();
        }
    }
}