﻿using System.Linq;
using Demos.WebAPI.DataAccess;
using Demos.WebAPI.Models;
using FluentAssertions;
using Xunit;

namespace Demos.WebAPI.Tests.DataAccess
{
    public class CustomerRepositorySpecification : AutoMockSpecification<CustomerRepository, ICustomerRepository>
    {
        public CustomerRepositorySpecification()
        {
            Chain<IUnitOfWorkFactory, IUnitOfWork>(x => x.Create());
        }

        [Fact]
        public void When_querying_by_city()
        {
            var customers = FakeMany<Customer>().AsQueryable();

            var expected = customers.First();

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Query<Customer>())
                .Returns(customers)
                .Verifiable());

            var result = Sut.GetByCity(expected.StreetAddress.City);

            result.Should().NotBeNull();
            result.Should().HaveCount(1);

            result.First().Should().Be(expected);

            Verify<IUnitOfWork>();
        }

        [Fact]
        public void When_registering_a_customer()
        {
            var customer = Fake<Customer>();

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Add(customer)).Verifiable());

            Sut.Register(customer);

            Verify<IUnitOfWork>();
        }

        [Fact]
        public void When_updating_customer_information()
        {
            var customer = Fake<Customer>();

            Dep<IUnitOfWork>(x => x.Setup(uow => uow.Update(customer)).Verifiable());

            Sut.UpdateInfo(customer);

            Verify<IUnitOfWork>();
        }
    }
}