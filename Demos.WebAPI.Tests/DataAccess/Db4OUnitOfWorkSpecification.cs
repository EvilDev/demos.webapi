﻿using System;
using Db4objects.Db4o;
using Demos.WebAPI.DataAccess;
using Demos.WebAPI.Models;
using Xunit;

namespace Demos.WebAPI.Tests.DataAccess
{
    public class Db4OUnitOfWorkSpecification : AutoMockSpecification<Db4OUnitOfWork, IUnitOfWork>
    {
        public Db4OUnitOfWorkSpecification()
        {
            Chain<IObjectServer, IObjectContainer>(x => x.OpenClient());
        }

        [Fact]
        public void When_adding_entity()
        {
            var entity = Fake<Customer>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Store(entity))
                                        .Verifiable());

            Sut.Add(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_updating_an_entity()
        {
            var entity = Fake<Customer>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Store(entity))
                                        .Verifiable());

            Sut.Update(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_deleting_an_entity()
        {
            var entity = Fake<Customer>();

            Dep<IObjectContainer>(x => x.Setup(oc => oc.Delete(entity))
                                        .Verifiable());

            Sut.Delete(entity);

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_disposing_without_error()
        {
            Dep<IObjectContainer>(x =>
            {
                x.Setup(oc => oc.Commit()).Verifiable();
                x.Setup(oc => oc.Close()).Verifiable();
            });


            Sut.Dispose();

            Verify<IObjectContainer>();
        }

        [Fact]
        public void When_disposing_with_error()
        {
            Dep<IObjectContainer>(x =>
            {
                x.Setup(oc => oc.Commit())
                    .Throws<Exception>()
                    .Verifiable();
                x.Setup(oc => oc.Rollback());
                x.Setup(oc => oc.Close());
            });

            Assert.Throws<Exception>(() => Sut.Dispose());

            Verify<IObjectContainer>();
        }
    }
}