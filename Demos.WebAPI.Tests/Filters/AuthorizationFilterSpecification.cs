﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Demos.WebAPI.Filters;
using FluentAssertions;
using Xunit;

namespace Demos.WebAPI.Tests.Filters
{
    public class AuthorizationFilterSpecification : AutoMockSpecification<AuthorizationFilter, IAuthorizationFilter>
    {
        [Fact]
        public async void When_passing_valid_credentials()
        {
            var context = Fake<HttpActionContext>();

            context.Request.Headers.Authorization = new AuthenticationHeaderValue("VerySecure");

            var expected = Fake<HttpResponseMessage>();

            var response = await Sut.ExecuteAuthorizationFilterAsync(context, Fake<CancellationToken>(), () => Task.Run(() => expected));

            response.Should().Be(expected);
        }

        [Fact]
        public async void When_passing_invalid_credentials()
        {
            var context = Fake<HttpActionContext>();

            var continuation = Fake<HttpResponseMessage>();

            var response = await Sut.ExecuteAuthorizationFilterAsync(context, Fake<CancellationToken>(), () => Task.Run(() => continuation));

            response.Should().NotBe(continuation);

            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
    }
}