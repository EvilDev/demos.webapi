﻿Feature: CustomersApi

	As a user,
	I would like to be able to query and modify customers in the system programmatically.

Background:
	Given I have an authorization token of "VerySecure"

Scenario: Getting customers by city
	When I get customers from "Winnipeg"
	Then there should be 50 results

Scenario: Register a customer
	Given I have the following customer
		| First Name | Last Name | Phone Number |
		| Bob        | Builder   | 123456789    |
	And the customer has the following street address
		| Address 1       | Address 2 | City    | Postal Code |
		| 123 Anywhere St | Apt. 503  | Toronto | A1A 1A1     |
	When I register the customer
	Then I should recieve a status code of "OK" 

Scenario: Register an invalid customer
	Given I have the following customer
		| Phone Number |
		| 123456789    |
	When I register the customer
	Then I should recieve a status code of "BadRequest"

Scenario: Update a customer
	Given I have the following customer
		| First Name | Last Name | Phone Number |
		| Bob        | Builder   | 123456789    |
	And the customer has the following street address
		| Address 1       | Address 2 | City    | Postal Code |
		| 123 Anywhere St | Apt. 503  | Toronto | A1A 1A1     |
	When I update the customers information
	Then I should recieve a status code of "OK"

Scenario: Update an invalid customer
	Given I have the following customer
		| Phone Number |
		| 123456789    |
	When I update the customers information
	Then I should recieve a status code of "BadRequest"