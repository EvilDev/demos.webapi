﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Demos.WebAPI.IntegrationTests.Steps
{
    public static class WebServer
    {
        public const string BaseUrl = "http://localhost:53141/";

        private static Process _iisProcess;

        public static void StartIis()
        {
            if (_iisProcess == null)
            {
                var thread = new Thread(StartIisExpress) {IsBackground = true};
                thread.Start();
            }
        }

        private static void StartIisExpress()
        {
            var websitePath = AppDomain.CurrentDomain.BaseDirectory;

            websitePath = Path.GetFullPath(websitePath + @"..\..\..\..\Demos.WebAPI");

            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Normal,
                ErrorDialog = true,
                LoadUserProfile = true,
                CreateNoWindow = false,
                UseShellExecute = false,
                Arguments = string.Format("/path:\"{0}\" /port:{1}", websitePath, "53141")
            };
            var programfiles = string.IsNullOrEmpty(startInfo.EnvironmentVariables["programfiles"])
                ? startInfo.EnvironmentVariables["programfiles(x86)"]
                : startInfo.EnvironmentVariables["programfiles"];
            startInfo.FileName = programfiles + "\\IIS Express\\iisexpress.exe";
            try
            {
                _iisProcess = new Process {StartInfo = startInfo};
                _iisProcess.Start();
                _iisProcess.WaitForExit();
            }
            catch
            {
                _iisProcess.CloseMainWindow();
                _iisProcess.Dispose();
            }
        }

        public static void StopIis()
        {
            if (_iisProcess != null)
            {
                _iisProcess.CloseMainWindow();
                _iisProcess.Dispose();
            }
        }
    }
}