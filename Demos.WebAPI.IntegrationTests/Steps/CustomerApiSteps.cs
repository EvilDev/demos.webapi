﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Demos.WebAPI.Models;
using EasyHttp.Http;
using FluentAssertions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;

namespace Demos.WebAPI.IntegrationTests.Steps
{
    [Binding]
    public class CustomerApiSteps
    {
        [Given(@"I have an authorization token of ""(.*)""")]
        public void GivenIHaveAnAuthorizationTokenOf(string authorizationKey)
        {
            var client = ScenarioContext.Current.Get<HttpClient>();

            client.Request.AddExtraHeader("Authorization", authorizationKey);
        }

        [When(@"I get customers from ""(.*)""")]
        public void WhenIGetCustomersFrom(string city)
        {
            var client = ScenarioContext.Current.Get<HttpClient>();

            var response = client.Get(string.Format("api/v1/customers/{0}", city));

            var results = response.StaticBody<Customer[]>();

            ScenarioContext.Current.Set(results.AsEnumerable());
        }

        [Then(@"there should be (.*) results")]
        public void ThenThereShouldBeResults(int resultCount)
        {
            var results = ScenarioContext.Current.Get<IEnumerable<Customer>>();

            results.Count().Should().Be(resultCount);
        }

        [Given(@"I have the following customer")]
        public void GivenIHaveTheFollowingCustomer(Table table)
        {
            var customer = table.CreateInstance<Customer>();
            ScenarioContext.Current.Set(customer);
        }

        [Given(@"the customer has the following street address")]
        public void GivenTheCustomerHasTheFollowingStreetAddress(Table table)
        {
            var address = table.CreateInstance<Address>();
            ScenarioContext.Current.Get<Customer>().StreetAddress = address;
        }

        [When(@"I register the customer")]
        public void WhenIRegisterTheCustomer()
        {
            var client = ScenarioContext.Current.Get<HttpClient>();
            var customer = ScenarioContext.Current.Get<Customer>();

            var response = client.Post("api/v1/customer", customer, "application/json");

            ScenarioContext.Current.Set(response);
        }

        [When(@"I update the customers information")]
        public void WhenIUpdateTheCustomersInformation()
        {
            var client = ScenarioContext.Current.Get<HttpClient>();
            var customer = ScenarioContext.Current.Get<Customer>();

            var response = client.Put("api/v1/customer", customer, "application/json");

            ScenarioContext.Current.Set(response);
        }


        [Then(@"I should recieve a status code of ""(.*)""")]
        public void ThenIShouldRecieveAStatusCodeOf(string statusCode)
        {
            var response = ScenarioContext.Current.Get<HttpResponse>();

            var status = Enum.GetName(typeof (HttpStatusCode), response.StatusCode);
            
            Assert.Equal(statusCode, status);
        }
    }
}