﻿using EasyHttp.Http;
using TechTalk.SpecFlow;

namespace Demos.WebAPI.IntegrationTests.Steps
{
    [Binding]
    public class TestRunnerConfig
    {
        [BeforeScenario]
        public static void Setup()
        {
            ScenarioContext.Current.Set(new HttpClient(WebServer.BaseUrl));
        }

        [BeforeTestRun]
        public static void RunIis()
        {
            WebServer.StartIis();
        }

        [AfterTestRun]
        public static void KillIis()
        {
            WebServer.StopIis();
        }
    }
}