﻿using System.Web;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Db4objects.Db4o;
using Demos.WebAPI.DataAccess;

namespace Demos.WebAPI.Installers
{
    public class DataAccessInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var path = string.Format(@"{0}\default.data", HttpContext.Current.Server.MapPath("App_Data"));

            container.Register(Component.For<IUnitOfWorkFactory>().AsFactory());

            container.Register(Component.For<IUnitOfWork>()
                                        .ImplementedBy<Db4OUnitOfWork>()
                                        .LifestylePerWebRequest());

            container.Register(Component.For<IObjectServer>()
                                        .UsingFactoryMethod(() => Db4oFactory.OpenServer(path, 8181)));

            container.Register(Classes.FromThisAssembly()
                                      .BasedOn(typeof(Repository<>))
                                      .WithServiceDefaultInterfaces());
        }
    }
}