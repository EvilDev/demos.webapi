﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Demos.WebAPI.FrameworkExtensions;

namespace Demos.WebAPI.Installers
{
    public class WebApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDependencyResolver>().ImplementedBy<WindsorDependencyResolver>());

            container.Register(Classes.FromThisAssembly()
                                      .BasedOn<ApiController>()
                                      .WithServiceSelf()
                                      .LifestylePerWebRequest());
        }
    }
}