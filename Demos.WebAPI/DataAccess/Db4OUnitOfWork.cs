using System;
using System.Linq;
using Db4objects.Db4o;
using Db4objects.Db4o.Linq;

namespace Demos.WebAPI.DataAccess
{
    public class Db4OUnitOfWork : IUnitOfWork
    {
        private readonly IObjectServer _server;
        private readonly IObjectContainer _container;

        public Db4OUnitOfWork(IObjectServer server)
        {
            _server = server;
            _container = server.OpenClient();
        }

        public IQueryable<TEntity> Query<TEntity>()
        {
            return _container.Cast<TEntity>().AsQueryable();
        }

        public void Add<TEntity>(TEntity entity)
        {
            _container.Store(entity);
        }

        public void Update<TEntity>(TEntity entity)
        {
            _container.Store(entity);
        }

        public void Delete<TEntity>(TEntity entity)
        {
            _container.Delete(entity);
        }

        public void Dispose()
        {
            try
            {
                _container.Commit();
            }
            catch (Exception)
            {
                _container.Rollback();
                throw;
            }
            finally
            {
                _container.Close();
            }
        }
    }
}