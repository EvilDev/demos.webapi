﻿using System.Linq;

namespace Demos.WebAPI.DataAccess
{
    public abstract class Repository<TEntity>
    {
        private readonly IUnitOfWorkFactory _uowFactory;

        protected Repository(IUnitOfWorkFactory uowFactory)
        {
            _uowFactory = uowFactory;
        }

        private IUnitOfWork Uow
        {
            get { return _uowFactory.Create(); }
        }

        protected IQueryable<TEntity> Query
        {
            get { return Uow.Query<TEntity>(); }
        }

        protected void Add(TEntity entity)
        {
            Uow.Add(entity);
        }

        protected void Update(TEntity entity)
        {
            Uow.Update(entity);
        }

        protected void Delete(TEntity entity)
        {
            Uow.Delete(entity);
        }
    }
}