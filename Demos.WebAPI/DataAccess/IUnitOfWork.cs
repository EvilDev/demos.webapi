﻿using System;
using System.Linq;

namespace Demos.WebAPI.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IQueryable<TEntity> Query<TEntity>();

        void Add<TEntity>(TEntity entity);

        void Update<TEntity>(TEntity entity);

        void Delete<TEntity>(TEntity entity);
    }
}