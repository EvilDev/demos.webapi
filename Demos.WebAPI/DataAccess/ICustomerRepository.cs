﻿using System.Linq;
using Demos.WebAPI.Models;

namespace Demos.WebAPI.DataAccess
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> GetByCity(string city);

        void Register(Customer customer);

        void UpdateInfo(Customer customer);
    }
}