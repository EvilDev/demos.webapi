﻿using System;
using System.Linq;
using Demos.WebAPI.Models;

namespace Demos.WebAPI.DataAccess
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IUnitOfWorkFactory uowFactory) : base(uowFactory)
        {
        }

        public IQueryable<Customer> GetByCity(string city)
        {
            return Query.Where(c => String.Equals(c.StreetAddress.City.Trim(), city.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public void Register(Customer customer)
        {
            Add(customer);
        }

        public void UpdateInfo(Customer customer)
        {
            Update(customer);
        }
    }
}