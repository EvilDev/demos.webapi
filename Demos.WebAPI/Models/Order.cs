﻿using System;

namespace Demos.WebAPI.Models
{
    public class Order
    {
        public DateTime DatePurchased { get; set; }

        public Address ShipTo { get; set; }

        public Address BillingAddress { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public Guid CustomerId { get; set; }
    }
}