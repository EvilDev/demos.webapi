﻿namespace Demos.WebAPI.Models
{
    public enum PaymentMethod
    {
        Paypal,
        Stripe,
        Visa,
        Mastercard,
        AmericanExpress
    }
}