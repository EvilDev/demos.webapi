﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Demos.WebAPI.Models
{
    public class Customer
    {
        public Guid Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public Address StreetAddress { get; set; }

        public string PhoneNumber { get; set; }
    }
}