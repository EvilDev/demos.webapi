﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Demos.WebAPI.DataAccess;
using Demos.WebAPI.Models;

namespace Demos.WebAPI.Controllers
{
    /// <summary>
    /// Provides access for retrieving and manipulating customer data.
    /// </summary>
    [Route("api/v1/customer")]
    public class CustomersController : ApiController
    {
        private readonly ICustomerRepository _repository;

        public CustomersController(ICustomerRepository repository)
        {
            _repository = repository;
        }
        
        /// <summary>
        /// Retrieve a list of customers by city
        /// </summary>
        /// <param name="city">City</param>
        /// <returns></returns>
        [Route("api/v1/customers/{city}")]
        public IQueryable<Customer> Get(string city)
        {
            return _repository.GetByCity(city);
        }

        /// <summary>
        /// Register a customer
        /// </summary>
        /// <param name="customer">Customer Information</param>
        /// <returns>
        /// OK for valid registration, BadRequest for invalid registrations
        /// </returns>
        public HttpResponseMessage Post(Customer customer)
        {
            if (ModelState.IsValid)
            {
                _repository.Register(customer);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }

        /// <summary>
        /// Update Customer Information
        /// </summary>
        /// <param name="customer">Customer Information</param>
        /// <returns>
        /// OK for valid registration, BadRequest for invalid registrations
        /// </returns>
        public HttpResponseMessage Put(Customer customer)
        {
            if (ModelState.IsValid)
            {
                _repository.UpdateInfo(customer);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        }
    }
}