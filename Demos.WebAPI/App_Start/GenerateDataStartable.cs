﻿using System;
using System.Web;
using Castle.Core;
using Db4objects.Db4o;
using Demos.WebAPI.DataAccess;
using Demos.WebAPI.Models;
using Ploeh.AutoFixture;
using System.IO;

namespace Demos.WebAPI
{
    public class GenerateDataStartable : IStartable
    {
        private readonly string _path;

        public GenerateDataStartable()
        {
            _path = string.Format(@"{0}\default.data", HttpContext.Current.Server.MapPath("App_Data"));
        }

        public void Start()
        {
            if(File.Exists(_path)) File.Delete(_path);

            var fixture = new Fixture();

            fixture.Customize<Address>(c => c.With(x => x.City, "Winnipeg"));
            var customers = fixture.CreateMany<Customer>(50);

            using (var factory = new UowFactory(_path))
            {
                var customerRepo = new CustomerRepository(factory);

                foreach (var customer in customers)
                {
                    customerRepo.Register(customer);
                }
            }
        }

        public void Stop()
        {
        }

        private class UowFactory : IUnitOfWorkFactory, IDisposable
        {
            private readonly string _path;
            private IUnitOfWork _uow;
            private IObjectServer _server;

            public UowFactory(string path)
            {
                _path = path;
            }

            public IUnitOfWork Create()
            {
                if (_uow == null)
                {
                    _server = Db4oFactory.OpenServer(_path, 8080);
                    _uow = new Db4OUnitOfWork(_server);
                }

                return _uow;
            }

            public void Dispose()
            {
                _uow.Dispose();
                _uow = null;
                _server.Close();
            }
        }
    }
}