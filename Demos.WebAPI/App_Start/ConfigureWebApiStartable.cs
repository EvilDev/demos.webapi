﻿using System.Web.Http;
using System.Web.Http.Dependencies;
using Castle.Core;
using Demos.WebAPI.Filters;

namespace Demos.WebAPI
{
    public class ConfigureWebApiStartable : IStartable
    {
        private readonly HttpConfiguration _config;
        private readonly IDependencyResolver _dependencyResolver;

        public ConfigureWebApiStartable(HttpConfiguration config, IDependencyResolver dependencyResolver)
        {
            _config = config;
            _dependencyResolver = dependencyResolver;
        }

        public void Start()
        {
            _config.DependencyResolver = _dependencyResolver;
            
            _config.Filters.Add(new AuthorizationFilter());

            _config.Filters.Add(new ExceptionFilter());

            _config.MessageHandlers.Add(new TracingFilter());

            _config.MapHttpAttributeRoutes();

            _config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        public void Stop()
        {
        }
    }
}
