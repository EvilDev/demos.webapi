﻿using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using Elmah;

namespace Demos.WebAPI.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public bool AllowMultiple { get { return false; } }
        
        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            return Task.Run(() => ErrorLog.GetDefault(HttpContext.Current).Log(new Error(actionExecutedContext.Exception)), cancellationToken);
        }
    }
}