﻿using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using WebGrease.Css.Extensions;

namespace Demos.WebAPI.Filters
{
    public class TracingFilter : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var requestDetails = GetRequestDetails(request);

            var requestContent = request.Content == null ? string.Empty : await request.Content.ReadAsStringAsync();
            LogManager.GetCurrentClassLogger().Trace("{0}\n\n{1}", requestDetails, requestContent);


            var response = await base.SendAsync(request, cancellationToken);

            var responseDetails = GetResponseDetails(response);
            var responseContent = response.Content == null ? string.Empty : await response.Content.ReadAsStringAsync();

            LogManager.GetCurrentClassLogger().Trace("{0}\n\n{1}", responseDetails, responseContent);

            return response;
        }

        private string GetRequestDetails(HttpRequestMessage request)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Request:");
            request.Headers.ForEach(header => sb.AppendLine(string.Format("{0}: {1}", header.Key, string.Join(";", header.Value))));

            return sb.ToString();
        }

        private string GetResponseDetails(HttpResponseMessage response)
        {
            var sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine("Response:");

            response.Headers.ForEach(header => sb.AppendLine(string.Format("{0}: {1}", header.Key, string.Join(";", header.Value))));
            return sb.ToString();
        }
    }
}