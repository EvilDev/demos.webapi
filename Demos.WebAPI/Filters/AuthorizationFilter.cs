﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Demos.WebAPI.Filters
{
    public class AuthorizationFilter : IAuthorizationFilter
    {
        public const string ValidCredential = "VerySecure";

        public bool AllowMultiple
        {
            get { return false; }
        }

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext,
            CancellationToken cancellationToken,
            Func<Task<HttpResponseMessage>> continuation)
        {
            var credentials = actionContext.Request.Headers.Authorization.Scheme;

            if (credentials == ValidCredential)
            {
                return continuation();
            }

            return Task.Run(() => new HttpResponseMessage(HttpStatusCode.Unauthorized)
                                        {
                                            Content = new StringContent("You are not authorized to access this resource")
                                        }, 
                                        cancellationToken);
        }
    }
}