using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Demos.WebAPI.Areas.HelpPage
{
    public class HelpPageAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "HelpPage";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "HelpPage_Default",
                "Help/{action}/{apiId}",
                new { controller = "Help", action = "Index", apiId = UrlParameter.Optional });

            HelpPageConfig.Register(GlobalConfiguration.Configuration);

            var documentpath = HttpContext.Current.Server.MapPath("App_Data");
            GlobalConfiguration.Configuration.SetDocumentationProvider(new XmlDocumentationProvider(string.Format(@"{0}\documentation.xml",documentpath)));
        }
    }
}