This project contains the source code for the WebApi Workshop.

You will need:

- Visual Studio 2012 or 2013
- Web Developer Tools Installed
- Familiarity with Nuget to install packages/third party dependencies
- SpecFlow extension for visual studio recommended to run Integration Tests
- Xunit Test Runner
	- Resharper 8 - Install Xunit extension via the plugin gui
	- Resharper 7 - Download and install Xunit.Contrib runner - [http://xunitcontrib.codeplex.com/releases]
	- Visual Studio Xunit Test Runner - http://visualstudiogallery.msdn.microsoft.com/463c5987-f82b-46c8-a97e-b1cde42b9099?SRC=VSIDE
	
Follow steps outlined on the Wiki to follow along with the talk.